In Image 1, which has a good farm-cover, we see an excellent coverage by the AK farmplots over the Google Maps data. 
Also, the (translated) GSDA cadastres seem to agree with the Google maps data reasonably well and for a large part, 
Q-polygons (blue) lie within a P-polygon (orange)