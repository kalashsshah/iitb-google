import psycopg2
import pandas as pd
conn = psycopg2.connect(
    host="localhost",
    database="PoCRA",
    user="postgres",
    password="")

conn_cursor=conn.cursor()

#super-polygon list. So each element of list contains the list of elements which form a super polygon
connected_component_list=[]

#Params
alpha_0=0.1
tab_1 = 'pocra'
tab_2 = 'mangrul_oct19'

#As in any particular BFS, we want to maintain a table/hash-map to check if 
#the polygon is a part of super-polygon
visited = {}

#Polygons in table 1 and 2 may have the same name, so it is important to add a suffix while differentiating them
sql_add_suffix_1='''
UPDATE {} SET name = CONCAT(name, '1'); 
'''.format(tab_1)
conn_cursor.execute(sql_add_suffix_1)

sql_add_suffix_2='''
UPDATE {} SET name = CONCAT(name, '2'); 
'''.format(tab_2)
conn_cursor.execute(sql_add_suffix_2)

#The combined data from timestamp 1 & 2. We can temporarily create a table p1_p2 and then delete it
#once the task is over
sql_make_table='''
    CREATE table joint_data(
        name VARCHAR(20) NOT NULL UNIQUE,
        geom geometry
    );
'''
conn_cursor.execute(sql_make_table)

sql_insert_data='''
    INSERT INTO joint_data
    SELECT name, geom  from {} 
    UNION ALL
    SELECT name, geom from {};
    '''.format(tab_1, tab_2)
conn_cursor.execute(sql_insert_data)

sql_read_data='''
    SELECT name, geom  from {} 
    UNION ALL
    SELECT name, geom from {};
    '''.format(tab_1, tab_2)
conn_cursor.execute(sql_read_data)
data=conn_cursor.fetchall()


#Initialising the dictionary
for i in range (len(data)):
    visited['{}'.format(data[i][0])] = False


########################################################################################################
#GETTING THE CONNECTED COMPONENTS
for i in range(len(data)):
    if(visited['{}'.format(data[i][0])] == True):
        continue

    else:
        #Contain all the elements of 1 super-polygons
        iter_super_polygon=[] 
        #We now run the popular BFS algorithm
        #The neighbour stack
        queue=[]
        queue.append(data[i][0])

        while(len(queue)>0):
            #The polygon who is at the helm
            base_polygon = queue.pop(0)
            if(visited['{}'.format(base_polygon)] == False):
                visited['{}'.format(base_polygon)] = True
                
            iter_super_polygon.append(base_polygon)


            #SQL query to get graph neighbours of base polygon of type-A
            sql_query_get_graph_neighbours='''

            Select Q.name AS name
            from joint_data as P
            inner join 
                joint_data as Q
            on
                P.name in ('{}')
            and
                ST_Area(P.geom)>0
            and
                ST_Area(Q.geom)>0

            WHERE 
                (ST_Area(ST_Intersection(P.geom, Q.geom)) / ST_Area(P.geom)>={} 
                OR ST_Area(ST_Intersection(P.geom, Q.geom)) / ST_Area(Q.geom)>={})
                AND P.name != Q.name;

            '''.format(base_polygon,alpha_0, alpha_0)
            conn_cursor.execute(sql_query_get_graph_neighbours)
            records=conn_cursor.fetchall()

            if(len(records)>0):
                for j in range(len(records)):
                    res_poly=records[j][0]
                    if(visited['{}'.format(res_poly)]==False):
                        queue.append(res_poly)
                        visited['{}'.format(res_poly)]= True

           
        connected_component_list.append(iter_super_polygon)

########################################################################################################
# #GENERATING THE SUPER-POLYGONS AND ADDING IT INTO POSTGRESQL
# Make the table
sql_make_super_table = '''
CREATE table super_polygons(
name VARCHAR(20),
geom geometry);
'''
conn_cursor.execute(sql_make_super_table)

super_polygon_idx=0 #The index of the super-polygon

for row in connected_component_list:
    if(len(row)>0):
        #for polygon in row

        sql_query_geom = '''
        Select ST_Union(joint_data.geom) from joint_data where name in ({});
        '''.format(str(row)[1:len(str(row))-1])
        conn_cursor.execute(sql_query_geom)
        super_polygon_geom=conn_cursor.fetchall()



      
        sql_query_insert_super_polygon='''
        INSERT into super_polygons values(
        {}, {});
        '''.format('polygon_{}'.format(super_polygon_idx), super_polygon_geom[0][0])
        conn_cursor.execute(sql_query_insert_super_polygon)


###########################################
#DELETE the p1_p2 table formed before
sql_drop_table='DROP TABLE joint_data;'
conn_cursor.execute(sql_drop_table)

conn.commit() # <--- makes sure the change is shown in the database
conn.close()







