Analysis of Google farm plot data, its comparison to PoCRA cadastral
data.

# Getting started

Start with the directory containing this git repository:

```
cd iitb-google
```

Extract the dump file using GNU unzip:

```
gunzip -kf file_46259c1d500ab38_postgis_dump.sql.gz
```

Use `psql` program to create the necessary schemas.  The file is named
`psql.exe` on Windows.

```
psql -d <dbname> -c "create schema osmanabad; create schema pocra_dashboard"
```

Use the same program to load the contents into PostgreSQL database.

```
psql -d <dbname> -f file_46259c1d500ab38_postgis_dump.sql
```

The command takes several seconds (or minutes depending on hardware).
Ensure that no errors are reported on the console.  Upon successful
execution of the above command, the table `google_cadastres_postgis`
(along with two more tables) should be created.  This table contains
farm plot data from Google.  The two other tables created are village
table and cadastres table for Osmanabad district.  At this point, the
database is all set for analysis, have fun with SQL, spatial joins and
such!

# Villagewise farm plots

The farm plot data contained within a whole GeoJSON file may seem
prohibitively large.  To facilitate analysis, farm plot data for a few
villages is extracted from the large file and added as PostgreSQL dump
files, within the `villagewise_farmplots` directory.
E.g. `villagewise_farmplots/farmplots_indapur.sql.gz` contains farm
plot polygons, cadastral polygons and LULC (land use land cover)
polygons for the village Indapur in Osmanabad district, Washi taluka.

Villagewise data can be loaded in PostgreSQL with the following
commands.  Create the necessary schema first.  Subsitute your database
name in place of "<dbname>":

```
psql -d <dbname> -c "drop schema if exists google_osmanabad cascade"
psql -d <dbname> -c "create schema google_osmanabad"
```

Load the village data.  This step needs to be executed only once, as
it loads the village polygons.

```
cd iitb-google/villagewise_farmplots
gunzip -kf villages.sql.gz
psql -d <dbname> -f villages.sql
```

Load each village's farmplot data.  The following commands illustrate
data loading for village Rui.  Repeat them for other villages,
adjusting the filename accordingly.

```
gunzip -kf farmplots_rui.sql.gz
psql -d <dbname> -f farmplots_rui.sql
```

At this point, the database `<dbname>` (read your database name used
in the commands above) is ready for analysis.  Open QGIS application
and connect to `<dbname>`.  Expand the schema `google_osmanabad`.
Load the following tables as vector layers in QGIS:

```
farmplots_rui (polygons from Google AK)
cadastres_rui (MRSAC cadastral polygons)
lulc_rui (MRSAC land use land cover)
villages
```